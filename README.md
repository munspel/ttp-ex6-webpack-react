# Using react

Установка реакт
```
npm install react react-dom --save
```
Для того чтобы React работал, необходимо вместе с ним установить Babel. Он поможет в транспайлинге ES6 и JSX в ES5.
Установите babel-core, babel-loader, babel-preset-env, babel-preset-react в качестве зависимости dev.

```
npm install @babel/core babel-loader @babel/preset-env @babel/preset-react --save-dev
```
- **babel-core**: Преобразует код ES6 в ES5.
- **babel-loader**: Помощник Webpack для транспайлинга кода, задает пресеты.
- **babel-preset-env**: Пресет, который помогает babel конвертировать код ES6, ES7 и ES8 в код ES5.
- **babel-preset-react**: Пресет, преобразующий JSX в JavaScript.

# .babelrc:
Теперь создайте файл .babelrc внутри корневого каталога проекта, со следующим содержимым:
```
{
"presets": ["@babel/preset-env", "@babel/preset-react"]
}
```
Данный файл сообщит babel, какие из пресетов использовать для транспайлинга кода. Здесь мы используем два пресета:

- **env**: Этот пресет используется для транспайлинга кода ES6/ES7/ES8 в ES5.
- **react**: Этот пресет используется для транспайлинга кода JSX в ES5.

Добавляем лоадер webpack
```html
    {
       test: /\.(js|jsx)$/,
       exclude: /nodeModules/,
       use: {
         loader: 'babel-loader'
       }
    },
```
