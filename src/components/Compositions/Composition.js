import React from "react";
import SimpleClassComponent from "./../Simplests/SimpleClassComponent";
import SimpleComponent from "./../Simplests/SimpleComponent";

export default function Copmosition(props) {
  const data = ["Petyj", "Vasy", "Dima"];
  return (
    <div className='composition'>
      <h1>Composition example</h1>
      <ul>
        <li>
          <SimpleClassComponent name="SimpleClassComponent"></SimpleClassComponent>
        </li>
        <li>
          <SimpleComponent name="SimpleComponent"></SimpleComponent>
        </li>
        {data.map((name) => {
          return <li><SimpleComponent name={name}></SimpleComponent></li>
        })}
      </ul>
    </div>
  );
}
