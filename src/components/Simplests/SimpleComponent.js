import React from "react";

export default function SimpleComponent(props) {
  return <div>SimpleComponent. Property name = {props.name}</div>;
}
