import React from "react";

export default class SimpleClassComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        SimpleClassComponent. Property name = {this.props.name}
      </div>);
  }
}
