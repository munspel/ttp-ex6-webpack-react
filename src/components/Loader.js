import React, {Component} from "react";

export default class Loader extends Component {
  render() {
    return (
      <div className="col d-flex justify-content-center">
        <div className="spinner-border m-5" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      </div>)
  }
}
