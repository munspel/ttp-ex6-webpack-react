import React, {Component} from "react";
import UserList from "./UserList";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: props.appTitle,
      name: "App"
    }
  }

  componentDidMount() {
    document.title = this.state.title;
  }

  componentDidUpdate() {
    document.title = this.state.title;
  }

  handleChangeTitle() {
    this.setState({
      name: "New name value",
      title: "New App title",
    });
    document.title = "New App title";
  }
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <h1 className="py-1 text-center">React api example</h1>
          </div>
        </div>
        <main className='row'>
          <div className="col">
            <UserList/>
          </div>
        </main>
      </div>
    );
  }
}

export default App;
