import React, {Component} from "react";
import usersServise from "../api/users.js";
import Loader from "./Loader";

export default class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      loading: true,
      users: [],
      user: null,
    }
  }

  componentDidMount() {
    document.title = "Загрузка списка ...";
    usersServise.fetch(this.state.currentPage)
      .then((response) => {
        this.setState({
          loading: false,
          users: response.data.data
        })
      });
  }

  componentDidUpdate() {
    if (this.state.loading) {
      document.title = "Загрузка списка";
    } else {
      document.title = "User list";
    }
  }

  handleView(item) {
    this.setState({user: item})
  }

  handleClose() {
    this.setState({user: null})
  }

  render() {
    const {loading, users, user} = this.state;
    if (loading) {
      return (
        <div>
          <h2>User list</h2>
          <div className="row">
             <Loader />
          </div>
        </div>
      )
    }
    let usersList = users.map((item) => {
      return (
        <div key={item.id} className="col-6 col-sm-4 col-md-4 col-lg-3 mb-3">
          <div className="card shadow">
            <figure>
              <img className="img-fluid w-100" src={item.avatar}/>
            </figure>
            <div className="card-body">
              <p className="card-text"><b>{item.first_name} {item.last_name}</b></p>
              <a href="#" className="btn btn-primary" onClick={this.handleView.bind(this, item)}>View details</a>
            </div>
          </div>
        </div>)
    });
    return (
      <div className="row">
        <div className="col">
          {user &&
          <div className="row">
            <div className="col">
              <h2>User details: {user.first_name}{user.last_name}</h2>
              <img className="rounded-circle" src={user.avatar}/>
              <a className="btn btn-secondary btn-small m-3" onClick={this.handleClose.bind(this)}>Close</a>
            </div>
          </div>
          }
          <div className="row">
            {usersList}
          </div>
        </div>
      </div>
    );
  }
}
