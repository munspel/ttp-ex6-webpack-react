import React, { Component } from "react";
export default class Person extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      active: false
    }
  }
  handleClick(){
    if(this.state.active){
      this.setState({active:false})
    } else {
      this.setState({active:true})
    }
  }
  render() {
    return (
      <div className="p-2">
        {this.props.name} -  {this.state.active ? "Да": "Нет"}
        <button className="btn btn-primary px-2" onClick={this.handleClick.bind(this)}>Активировать</button>
      </div>
    );
  }
}
