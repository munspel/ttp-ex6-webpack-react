import "./Timer.scss";
import React from "react";

export default class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.timerId = null;
    this.state = {
      countdown: props.seconds
    }
  }

  componentDidMount() {

    this.timerId = setInterval(() => {
      let current = this.state.countdown;
      if (current >= 0) {
        this.setState({countdown: current})
      } else {
        this.setState({countdown: 'Done'})
      }
    }, 1000);

  }

  componentDidUpdate() {
    console.log("updated: ", this.state)
  }

  componentWillUnmount() {
    clearInterval(this.timerId);
  }

  render() {
    return (
      <div className="countdown">
        <div className="countdown__number">{this.state.countdown}</div>
        {this.state.countdown > 0 && (
          <svg>
            <circle r="18" cx="20" cy="20"
                    style={{
                      animation: `countdown ${this.props.seconds}s linear infinite forwards`,
                      strokeWidth: `${this.props.lineWidth ?? 2}px`
                    }}></circle>
          </svg>
        )}
      </div>
    )
  }
}
