import React, { Component } from "react";
import Person from "./Person";
export default class List extends Component {
  render() {
    return (
      <div>
        <h1>Список покупок для {this.props.name}</h1>
        <ul>
          <li><Person name="Вася" /></li>
          <li><Person name="Петя" /></li>
          <li><Person name="Дима" /></li>
        </ul>
      </div>
    );
  }
}
