import http from "../utils/http";

export default {
  fetch: function (page){
    return http.get(`https://reqres.in/api/users?page=${page}`)
  },
  getById: function(id){
    return http.get(`https://reqres.in/api/users/${id}`)
  },
}

