import "./scss/index.scss";
import React from "react";
import ReactDOM from 'react-dom';
import Timer from "./components/Timer";
import App from "./components/app"
//1 - The Simplest react component
// ReactDOM.render(<SimpleComponent name="Алиса" />, document.getElementById("root"));
//2 - The Simplest class react component
// ReactDOM.render(<SimpleClassComponent name="Алиса" />, document.getElementById("root"));
//3 - Component composition
//ReactDOM.render(<Composition/>, document.getElementById("root"));
//4 - Component life cycle/state
// ReactDOM.render(
//   (<div>
//     <Timer seconds="5" lineWidth={2}/>
//     <Timer seconds="10" lineWidth={3}/>
//     <Timer seconds="15" lineWidth={4}/>
//   </div>), document.getElementById("root"));
// 5. Events
ReactDOM.render(<App appTitle="React APP" />, document.getElementById("root"));
